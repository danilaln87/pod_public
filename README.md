Что сделано:
**Главная страница** - /
- Добавлен catalog.smart.filter умный фильтр битрикс подтягивающий значения свойств товаров
- Стоимость и возраст беруться из заранее заданных интервалов. Возраст от 0 до 100 лет. Стоимость от 500 до 100000 меняются значения внутри фильтра

**Страница подарков** - /gifts/
Содержится скрытый фильтр для фильтрации товаров при переходе с фильтра на главной странице. Также содержится частичный фильтр при переходе по ссылке сгенерированной в **генераторе фильтров**. При переходе на страницу без фильтра отображаются все подарки из базы. Также сверху отображаются теги подарков, в данный момент беруться теги всех подарков в базе. При переходе по тегу - подарки фильтруются по данному значению тега у подарков. У товара есть 2 кнопки. "В избранное" добавляет товар в список избранного (находящийся на отдельной странице) и кнопка, которая принимает значение заданное в админке для подарка. Кнопка может быть:
- "подробнее" - клик по ней осуществляет переход на отдельную подробную страницу подарка, 
- "Cсылка на магазин" - переходит по url заданному в админке для подарка
- "Поискать на Яндексе" - осуществляет переход в яндекс и ищет подарок по его названию
Также у товара отображается дата добавления и его личные теги.

Подарок может быть как отдельным товаров имеющим отдельную страницу, так и товаром содержащим торговые предложения. Пример подарка с торговыми предложениями
http://pod.websputnik.su/gifts/muzhchinam/23-fevralya/puteshestviya/при переходе отображает список торговых предложений и можно перейти по ссылке и просмотреть конкретное предложение

Второй вариант товара обычная страница
http://pod.websputnik.su/gifts/muzhchinam/tyulpany-krasnye/
Подарок в админке создается стандартно. Если нужно создать подарок с торговыми предложениями то нажимаем эту кнопку

![screenshot of sample](https://downloader.disk.yandex.ru/preview/7cc8a15ab43f4075537b7bf28016939b254cd7345a050e6c86791f2b41790735/5e186445/teWNlGENaHoiNw3jWM6eXfjIlRUtaTm4-aCq3udTf0CYkgGjI1ZaEO8GeM1Lgcczdbp7iim7_fc_b_19lF3MEQ==?uid=0&filename=2019-11-26_16-47-39.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048) 

и далее заполняем поля и во вкладке "торговые предложения" также стандартно добавляем предложения. Каждое торговое предложение это отдельный товар. Пример. Создали товар "Путешествия" и для него 3 торговых

![screenshot of sample](https://downloader.disk.yandex.ru/preview/d01f0e336bc039513e0c2aa0aef04c09a0de5c413392f7f177dd5e6a5ac9eecd/5e186588/mGllfJDx3mkKwazrP0j4XVHGnBNfcVZHTBKRB6E1l5Lq-Vu1rSWTZoZIQl_N-fI8BjAUteYO3WYwbT3EaEE4hw==?uid=0&filename=2020-01-10_11-52-24.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

на сайте путешествие будет выглядеть так

![screenshot of sample](https://downloader.disk.yandex.ru/preview/d4f85808419d8f86f20ab02fbbde54ca1ce49d2013ade82ffb0e90f70d2de6a0/5e1865db/JPwRPa1FAF-lEoEGeSPZzSG7j6ziEQ3cEbJYd46FubWotUc3VbZ1719soaqoDjC6ARfPN373PXi-wOUJQl9pdA==?uid=0&filename=2020-01-10_11-53-53.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

при переходе в карточку будут отображаться отдельно торговые -

![screenshot of sample](https://downloader.disk.yandex.ru/preview/c735908f6cf92290ab6d05e69acb12729b0741a37011d82f1f41579d005a4d63/5e1865f6/w7QmRWYnwGC7XATKDRwFbrnddFZSyoGIP9unc2xX95U0wCYBtnzdYKo42upKscGUtQpEJg8VqkFqYWubbqzZZQ==?uid=0&filename=2020-01-10_11-54-18.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

Теги для подарка заполняются через запятую (без пробелов) - 

![screenshot of sample](https://downloader.disk.yandex.ru/preview/e0e78bd701398969b77801e3a3c86c7228795d1ea42c08e1452e0aaea0310857/5e18644d/DWMHEvFzPYUmMwkUQoI5pUhskEqEAfCJZLvPnG5agt6QLFYmwhsGaCpGxR-uCm9xBNn3gXTSi0503ikKGvl4ng==?uid=0&filename=2019-11-26_16-48-55.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048) 

тегом может быть как слово, так и словосочетание.



**Страница избранное** - /favorites/
Содержит список подарков добавленных в избранное. Товары можно удалить или просмотреть


**Генератор фильтров**  - http://pod.websputnik.su/bitrix/admin/settings.php?lang=ru&mid=filter_constructor&mid_menu=1страница со списком фильтров
http://pod.websputnik.su/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&amp;type=SERVICE&a...

Генератор фильтров содержит список всех свойств добавленных для инфоблока товаров. В генераторе можно отменить галочками нужные значения свойств участвующих в фильтрации. Также отметить какие свойства выводить в мини-фильтре для дальнейшей фильтрации на странице подарков. Также можно выбрать указывать в фильтре конкретный возраст или диапазон от и до. Выбрав нужные значения генератор создает ссылку фильтра с заданными значениями вида 
http://pod.websputnik.su/gifts/?filter=861216073, где число это хеш ID фильтра, домена и текущей даты, чтобы скрыть реальный ID и защитить фильтр от подбора конкурентами. У каждого фильтра соответственно число уникальное и не порядковое. Т.е. пример: Первый фильтр имеет хеш 481902735 следующий 861216073 и т.д.


**Далее по меню в админке** - все пункты тут - 

![screenshot of sample](https://downloader.disk.yandex.ru/preview/4c5731baff67f928d8599e4fa02785ea27fbf0e0112dbff7aa71059b44660720/5e186462/vIC3fWJrXhYBnxjeA0ko_hJ_TvvlHRY0AjpJkg5eyAIlqCsMqoEj5DEamKyxFCNZss8KsuASlLVUr1VA09xjrA==?uid=0&filename=2020-01-10_11-17-58.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)


Далее описание пунктов:

**Список подарков** -  редирект на инфоблок с товарами (подарками). Тут думаю все понятно

**Список фильтров** - редирект на инфоблок с фильтрами. Вручную там можно настроить следующий функционал:

- ![screenshot of sample](https://downloader.disk.yandex.ru/preview/f04b2d250c96b4f017fd3a8b0bc8060b3099d8fd9038fb682b56f9ed065a241f/5e18650a/Y5YDdvFyLn2bmlcGSCiq_iG7otfROvvsnpPXCwP3geQgGcBiiUW5u7dd7WSpWzmqQeWvj4DtMuHF_FIUYsoNkA==?uid=0&filename=2020-01-10_11-19-28.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

- Название фильтра
- URL фильтра - можно скопировать, чтобы использовать в тексте. Изменять нельзя! Т.к. хеш фильтра содержит нужные данные
- Синонимы - привязка элементов из инфоблока синонимы. В данном месте можно к фильтру привязать нужные синонимы
- Ключевики - привязка ключевиков из списка запросов поиска. Если нажать кнопку "выбрать" выпадет список ключевиков из запросов поиска. Их можно фильтровать по имени. 
Далее выбираем нужные (проставляем галочки) и жмем действие и добавить - 

![screenshot of sample](https://downloader.disk.yandex.ru/preview/54e5496cf0a8692f3caf429374474020e23210be860020d0fb0a12a74f73f270/5e186396/fAXZY-Lzwf00mRQO8nGe61HlhnqGl_-8YO6G8oi2SJEhkkpROjJZ4pGd6inAoAXaW4A3JVE04d5gmmoYOF0g2g==?uid=0&filename=2020-01-10_11-29-45.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048) 

Также ключевики можно вводить вручную, **в качестве разделителя использовать запятую**!

**Список поисковых фраз ** - выводится список слов использующихся пользователями в поиске на сайте. Чтобы добавить новые (вдруг надо) можно на сайте в поиске вбить слово и оно появиться в списке - !

![screenshot of sample](https://downloader.disk.yandex.ru/preview/155cd9d4ad4e982a7d382454943835ce4676e2a31a7617f43c4090b8353b36a7/5e18646c/y3XRL02tBaTpaNIS8vVkwhQn0xP_dDIFEVontqjzYchfbq-YfuvtubNZilf6uH6ubRFQuY-Omv9eXnQOgVqEWQ==?uid=0&filename=2020-01-10_11-31-59.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

 при клике на слово, открывается инструмент добавления синонима к слову. **ВАЖНО!** Синоним должен содержать и текущее слово, пример:
кликаем в списке на фразу "зеленый паровоз" - 

![screenshot of sample](https://downloader.disk.yandex.ru/preview/c1e1ae8264d26bfd7c7e9bcd410e2c09ffacc8a7183c8769b25155fa9d118a3d/5e186476/-GTCKhbFu9JY-cT44Fo-mhzEVDJHRNBo970_HFZivPFflIHVahK-P-JMbZQwIvRv9KATAlI_xyR9CDhMfLleTg==?uid=0&filename=2020-01-10_11-32-55.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

 открывается инструмент добавления синонима - 
 
 ![screenshot of sample](https://downloader.disk.yandex.ru/preview/9021b6df930fff13446cb916b553e0fcd2d205b0d4d4bb77d3be5827a261178b/5e18647e/jKJtI4rO-W_705vsTLXU3Wuxg85Am2M9-uYtwtDxSbHzC8yCPsfA-HaS5KOWUNSR05BrNsevSDvgqGBAj5Ldrw==?uid=0&filename=2020-01-10_11-33-25.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)
 
  и тут нужно задать фильтр для синонима (из инфоблока фильтров) и добавить ключевые запросы для синонима **ВКЛЮЧАЯ** наше слово. Т.е. должно быть так - 
  
  ![screenshot of sample](https://downloader.disk.yandex.ru/preview/e5ccfc2350b8250b954632c5aa603bc28c390d5f497cbdf79f476cb0c4e7ed99/5e186486/TN9wVwQpVD5sUISC16oq0kl7IzeWdBKtslG6Jh0SM_oNgBYaqqSXHqBpjTNwIpfMLNAqxRtM6ScChIc7wT17TA==?uid=0&filename=2020-01-10_11-34-30.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

**ВАЖНО!** ключевое слово должно **ВСЕГДА ** заканчиваться запятой, даже если после него больше ключевиков не добавляется. Также в списке поисковых фраз выводятся следующие параметры - 

![screenshot of sample](https://downloader.disk.yandex.ru/preview/18f0b0d7f14da409d64f87af9d7a2cbcf71cc306239a203dae89f0b10abe3953/5e18648f/y3XRL02tBaTpaNIS8vVkwm1keZdxy-stgONPQkzinnBSMzIRmun1OKpUJBRgdF2me3LWLuW5j20gAzPjPfD8rg==?uid=0&filename=2020-01-10_11-40-23.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=22806451&size=2048x2048)

      
т.е. отображается сколько раз вводили данный ключевик (учитываются уникальные запросы, т.е. 1 пользователь = 1 запрос). Показывает есть ли синоним у данного ключевика (но не учитывает, ввели ли вручную данный запрос в каком-то синониме, только синоним созданный конкретно для этого ключевика), также показывает используется ли ключевик уже в каком-то фильтре.


**Синонимы** - содержат список поисковых фраз, обьединеных общим списком. Разделитель в синонимах запятая. Список синонимов можно посмотреть отдельно по ссылке
http://pod.websputnik.su/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=9&type=SERVICE&lang=ru&apply_filter=Y

**Поиск на сайте** поиск на сайте идет в 2 этапа. 
1) Проверяется введенное слово среди ключевиков введеных в синонимах, если находит берет привязанный к синониму фильтр
2) Если синоним не найдет проверяется список ключевиков привязанных к самому фильтру. Если находит берет фильтр.
3) Если не найден ключевик не в синонимах не в фильтрах - выводит все товары!








